<?php
/*
Plugin Name: WordPress login Master
Description: This plugin is for user login
Author: Agile Solutions PK
Version: 1.1
Author URI: http://agilesolutionspk.com
*/
if ( !class_exists( 'WordPress_Login_Master' )){
	class WordPress_Login_Master{
		function __construct(){
			add_action( 'admin_menu', array(&$this, 'admin_menu') );
			add_action( 'init', array(&$this,'aspk_wp_login_post' ));
			//add_action( 'wp_ajax_my_ajax_call', array(&$this,'my_ajax_fun' ));
			add_filter('manage_users_columns',array(&$this, 'add_login_out_coloum'));
			add_action('admin_enqueue_scripts', array(&$this, 'scripts_method') );
			add_filter('manage_users_custom_column', array(&$this, 'add_to_user_colomn_value') , 99 , 3);
			add_action( 'wp_ajax_aspk_lock_unlock', array(&$this,'lock_unlock' ));
			add_action( 'wp_ajax_aspk_del_data', array(&$this,'delete_row_ajax' ));
			add_action( 'user_register', array(&$this,'myplugin_registration_save'), 10, 1 );
			add_filter('manage_edit-aspkuser_columns', array(&$this,'add_new_login_columns'));
			add_action('manage_posts_custom_column',array(&$this,'manage_gallery_columns'), 10, 2);
			add_action('clear_auth_cookie', array(&$this,'logout_check'), 99);
			add_action('wp_login', array(&$this,'login_check' ),99 , 2);
			register_activation_hook( __FILE__, array($this, 'install') );
			//add_filter( 'heartbeat_received', array(&$this,'acme_heartbeat_receive'), 10, 2 );
			//add_filter( 'heartbeat_nopriv_received', array(&$this,'acme_heartbeat_receive'), 10, 2 ); 
			add_action( 'heartbeat_nopriv_tick', array(&$this,'acme_heartbeat_receive'), 10, 2 ); 
			add_action( 'heartbeat_tick', array(&$this,'acme_heartbeat_receive'), 10, 2 ); 
			add_action('admin_init', array(&$this,'wpse_11244_restrict_admin'), 99);
			add_filter('login_message', array(&$this,'custom_login_message'));
			//add_action('admin_footer',  array(&$this,'heartbeat_control'));
			//add_action('wp_footer',  array(&$this,'heartbeat_control'));
			add_action( 'wp_ajax_nopriv_heartbeat-control', array(&$this,'check_ajax' ));
			add_action( 'wp_ajax_heartbeat-control', array(&$this,'check_ajax' ));
		}
		
		function delete_row_ajax(){
			$rs = $this->delete_row1($_POST['id']);
			if($rs == true) echo 'yes';
			exit;
		}
		
		function acme_heartbeat_receive( $response, $data ) {
			$user_id = get_current_user_id();
			$this->edd_heartbeat_received($user_id);
			return $response;
		} // end live_heartbeat_receive 
		
		function update_close_window_user(){
			$login_user = get_option('aspk_login_user', null);
			if($login_user != null){
				foreach($login_user as $key => $v){ // $key = user_id AND $v = time
					$userlogin_time = time() - $v;
					if ($userlogin_time > 70){
						$row_id = $this->select_last_row_id($key);
						$time_diff = $v - $row_id->login_time; 
						$this->insert_logout_time($v, $row_id->id , $time_diff);
						unset($login_user[$key]);
					}	
				}
				update_option('aspk_login_user' ,$login_user , true);
			}
		}
		
		function custom_login_message() {
				$message = '<p class="message">Due to locked By Admin You are not allowed to access this part of the site.</p><br />';
				return $message;
		}
		
		function manage_gallery_columns($column_name, $post_id) {
			$this->update_close_window_user();
			$rs = $this->select_row($post_id);
			$userinfo = get_user_by('id' , $rs->user_id);
			if($column_name == 'user_id'){
				?><a href = "<?php echo admin_url('user-edit.php?user_id='.$rs->user_id.'&wp_http_referer=%2Fqa11%2Fwp-admin%2Fusers.php');?>" ><?php echo $rs->user_id; ?></a><?php
			}
			if($column_name == 'use_date') echo date("Y-m-d", $rs->login_time);
			if($column_name == 'role') echo $userinfo->roles[0];
			if($column_name == 'login') echo date("H:i:s", $rs->login_time); 
			if($column_name == 'logout') echo date("H:i:s", $rs->logout_time);
			if($column_name == 'aspk_time') echo date("H:i:s", $rs->aspk_time);
			if($column_name == 'del') {
					?><img id="del_data" onclick = "del_post(<?php echo $post_id;?>);" style="width:2em;" src = "<?php echo plugins_url('img/del.jpg',__FILE__); ?>" /><?php
			}
			?><script>
				function del_post(id){
				
						var data = {
						'action'   : 'aspk_del_data',
						'id' : id,
				
						};
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
							jQuery.post(ajaxurl, data, function(response) {
								if(response == 'yes'){
									window.location.href = '<?php echo admin_url('edit.php?post_type=aspkuser'); ?>';
								}
							});	
					
				}
			</script><?php
		}
		function select_row($post_id){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}_aspk_trace_time where post_id = {$post_id}";
			return $wpdb->get_row($sql);
		}
		
		function wpse_11244_restrict_admin(){
			$user_id = get_current_user_id();
			$get_values = get_user_meta($user_id, '_aspk_lock_unlock');
			if ($get_values[0] == 'on' && ! current_user_can( 'manage_options' ) ) {
				$row = $this->select_last_row_id($user_id);
				$this->delete_row($row);	
				wp_logout();
			}
		}
		
		function delete_row($row){
			global $wpdb;
				if($row->logout_time == null){
					 wp_delete_post( $row->post_id );
					$sql = "DELETE FROM {$wpdb->prefix}_aspk_trace_time WHERE id = '{$row->id}'";
					return $wpdb->query($sql);
				}
		}
		
		function delete_row1($post_id){
			global $wpdb;
			
			wp_delete_post( $post_id );
			$sql = "DELETE FROM {$wpdb->prefix}_aspk_trace_time WHERE post_id = '{$post_id}'";
			return $wpdb->query($sql);
		}
		
		function scripts_method(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_script( 'heartbeat' );
		}
		
		function myplugin_registration_save( $user_id ) {
		
			$get_setting = get_option('aspk_lock_settings');
			$aspk = update_user_meta( $aspk_id, '_aspk_lock_unlock', $get_setting );
		}
		

		function edd_heartbeat_received( $user_id ) {
			$login_user = array();
			$login_user = get_option('aspk_login_user', null);
			$login_user[$user_id] = time();
			update_option('aspk_login_user',$login_user  , true);
			//$this->update_close_window_user();
		}
		
		function check_ajax(){
			$this->edd_heartbeat_received( $_POST['id'] );
			echo 'yes';
			exit;
		}
		
		function heartbeat_control(){
			//if(! user_logged_in()) return;
			?>
				<script>
					  //setInterval(function() { 
					   //jQuery(document).ready(function() {	
							var data = {
							'action'   : 'heartbeat-control',
							'id' : <?php echo get_current_user_id(); ?>,
					
							};
							var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
								// We can also pass the url value separately from ajaxurl for front end AJAX implementations
								jQuery.post(ajaxurl, data, function(response) {
									
							});
						// });
					//}, 30000); 
				</script>
			
			<?php
		}
		
		
		
		function install(){
			global $wpdb;
            
			update_option('aspk_lock_settings','off');
            $sql="
                CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."_aspk_trace_time` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `user_id` int(11) NOT NULL,
                  `post_id` int(11) NOT NULL,
                  `login_time` bigint NULL,
                  `logout_time` bigint NULL,
                  `aspk_time` bigint  NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
            ";
            $wpdb->query($sql);
			/* $page = get_page_by_title( 'Restriction' );
			if(! $page){
				$this->aspk_insert_page(get_current_user_id());
			}elseif($page->post_status == 'trash'){
				$my_post = array(
					  'ID'           => $page->ID,
					  'post_status' => 'publish'
				  );
				wp_update_post( $my_post );
			} */
		}
		
		function aspk_wp_login_post() {
			$labels = array(
				'name' => _x('Login Trail', 'post type general name'),
				'singular_name' => _x('Login Trail', 'post type singular name'),
				'add_new' => _x('Add New', 'user'),
				'add_new_item' => __("Add New user"),
				'edit_item' => __("Edit user"),
				'new_item' => __("New user"),
				'view_item' => __("View user"),
				'search_items' => __("Search user"),
				'not_found' =>  __('No user found'),
				'not_found_in_trash' => __('No user found in Trash'), 
				'parent_item_colon' => ''
			);
			$args = array(
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'Login_Date' ),
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => null,
				'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
				'capabilities' 		 => array('create_posts' => false)
			);
			  register_post_type('aspkuser',$args);
		}
		
		function add_new_login_columns($gallery_columns) {
			
			$new_columns['cb'] = '<input type="checkbox" />';	
			$new_columns['user_id'] = _x('User ID', 'column name');	
			$new_columns['use_date'] = _x('Login Date', 'column name');	
			$new_columns['role'] = _x('Role', 'column name');	
			$new_columns['login'] = _x('Login', 'column name');	
			$new_columns['logout'] = _x('Logout', 'column name');
			$new_columns['aspk_time'] = _x('Total time', 'column name');
			$new_columns['del'] = _x('Delete', 'column name');	
		 
			return $new_columns;
		} 
		function login_check($user, $detail){
			$this->update_close_window_user();
			$get_values = get_user_meta($detail->ID, '_aspk_lock_unlock');
			if ($get_values[0] == 'off'){
				$post_id = $this->aspk_insert_post($detail->ID);
				$this->insert_login_time($post_id ,$detail->ID, time());
			}elseif($get_values[0] == 'on' && $detail->roles[0] != 'administrator'){
				/*$page = get_page_by_title('Restriction');
				alog('$page',$page,__FILE__,__LINE__);
				$p_url = $page->guid;
				alog('$p_url',$p_url,__FILE__,__LINE__);
				wp_redirect($p_url);
				alog('$t ',$t ,__FILE__,__LINE__);*/
				wp_logout();
			}
		}
						
		
		function logout_check(){
			$user_id = get_current_user_id();
			$rs = $this->select_last_row_id($user_id);
			$this->insert_logout_time(time() , $rs->id , time()-$rs->login_time);
		}
						
			function admin_enqueue_scripts(){
			wp_enqueue_style( 'wp_login_master_bootstrap', plugins_url('css/agile-bootstrap.css', __FILE__) );	
		}
		
		function insert_login_time($post_id, $user_id, $login_time){
			global $wpdb;
			
			$sql = "INSERT INTO {$wpdb->prefix}_aspk_trace_time(user_id,post_id,login_time) VALUES({$user_id},'{$post_id}',{$login_time})";
			$wpdb->query($sql);	
		
		}
		
		function insert_logout_time($logout_time, $id , $time_diff){
			global $wpdb;
			
			$sql = "UPDATE {$wpdb->prefix}_aspk_trace_time set logout_time = '{$logout_time}' , aspk_time = '{$time_diff}'  where id = '{$id}'";
			$wpdb->query($sql);	
		}
		
		function select_last_row_id($user_id){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}_aspk_trace_time where user_id = {$user_id} ORDER BY id DESC LIMIT 1";
			return $wpdb->get_row($sql);
		}
		
		function add_to_user_colomn_value($empty, $column_name, $user_id){
			$get_setting = get_option('aspk_lock_settings');
			$get_values = get_user_meta($user_id, '_aspk_lock_unlock');
			if(empty($get_values)) update_user_meta($user_id, '_aspk_lock_unlock', $get_setting );
			?>
				<script>
					function locked_unlocked(id){
						var data = {
						'action'   : 'aspk_lock_unlock',
						'id' : id,
				
						};
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
							// We can also pass the url value separately from ajaxurl for front end AJAX implementations
							jQuery.post(ajaxurl, data, function(response) {
								jQuery('#aspk_user'+ id).attr('src', response);
							});
					
					}
				</script>
			<?php
			if ( $column_name == 'Log_Setings') {
				$setting = get_user_meta($user_id, '_aspk_lock_unlock');
				if($setting[0] == 'on'){
					$img_url = plugins_url('img/lock.jpg', __FILE__);

				}else{
					$img_url = plugins_url('img/unlock.jpg', __FILE__);
				}
				return $column_name = '<img style="width:2em;" id = "aspk_user'.$user_id.'" onclick="locked_unlocked('.$user_id.');" src = "'.$img_url.'" />';
			}
		}
		
		function lock_unlock(){
			$aspk_id = $_POST['id'];
			$get_radio_values = get_user_meta($aspk_id, '_aspk_lock_unlock');
			if($get_radio_values[0] == 'on'){
				update_user_meta($aspk_id, '_aspk_lock_unlock', 'off' );
				echo $img_url = plugins_url('img/unlock.jpg', __FILE__);
			}elseif($get_radio_values[0] == 'off'){
				update_user_meta( $aspk_id, '_aspk_lock_unlock', 'on' );
				echo plugins_url('img/lock.jpg', __FILE__);
			}
			exit;
		}
		
		function add_login_out_coloum($columns){
		
			$columns['Log_Setings'] = 'Locked/Unlocked';
			return $columns;
		}
		
		function admin_menu() {
			add_options_page('User setting','User setting','manage_options','aspk_wplm_settings',array( $this, 'group_settings' ));
		}
		
		function aspk_insert_post($user_id){
			$post = array(
			   'post_content'   =>  'Word Press Login',
			   'post_name'  	=> 'aspk_login' , // The name (slug) for your post
			   'post_title' 	=> 'Aspk Login' , // The title of your post.
			   'post_status'	=> 'publish' , // Default 'draft'.
			   'post_type'  	=>  'aspkuser'  ,// Default 'post'.
			   'post_author'	=>  $user_id , // The user ID number of the author. Default is the current user ID.
			   'post_date'  	=>  date('Y-m-d H:i:s') , // The time post was made.
			   'post_date_gmt'  =>  date('Y-m-d H:i:s')  // The time post was made, in GMT.
			);  
			$post_id = wp_insert_post( $post);
			return $post_id;
		}

		function aspk_insert_page($user_id){
			$post = array(
			   'post_content'   =>  'You are restrict by Admin and not allowed to access this part of the site.',
			   'post_name'  	=> 'Restriction' , // The name (slug) for your post
			   'post_title' 	=> 'Restriction' , // The title of your post.
			   'post_status'	=> 'publish' , // Default 'draft'.
			   'post_type'  	=>  'page'  ,// Default 'post'.
			   'post_author'	=>  $user_id , // The user ID number of the author. Default is the current user ID.
			   'post_date'  	=>  date('Y-m-d H:i:s') , // The time post was made.
			   'post_date_gmt'  =>  date('Y-m-d H:i:s')  // The time post was made, in GMT.
			);  
			$post_id = wp_insert_post($post);
			return $post_id;
		}
		
		function group_settings(){
			if(isset($_POST['submit'])){
				$aspk_radio = $_POST['user'];
				$as = update_option('aspk_lock_settings',$aspk_radio);
				?><h2 style = "color:green;">User Setting Has Been Saved.</h2><?php
			}
			$get_radio_values = get_option('aspk_lock_settings'); 
		
			?>
			 <div class="tw-bs container" style="background-color:white; width:90%; padding:1em;margin-top:1em;">
				<form method="post" action="" >
					<div class="row" style="clear:left;">
						<div class="col-md-12" style="margin-top:1em;"><h1>Default For New User</h1></div>
					</div>
						  <div class="row" style="clear:left;">
							<div class="col-md-4" style="margin-top:1em;">
								 <input type="radio" <?php checked($get_radio_values,'on');?> name="user" value="on">Locked
							</div>
						 </div>
						 <div class="row" style="clear:left;" style="margin-top:1em;">
							<div class="col-md-4" style="margin-top:1em;">
								<input type="radio" <?php checked($get_radio_values,'off');?> name="user" value="off">Open
							</div>
						 </div>
						 <div class="row" style="clear:left;" style="margin-top:1em;">
							<div class="col-md-12" style="margin-top:1em;">
								<input class = "btn btn-default" type="submit" name = "submit" value = "Save" />
							</div>
						 </div>
				</form>
			 </div>
			 
			<?php
		}
	}

 }	

new WordPress_Login_Master();	
